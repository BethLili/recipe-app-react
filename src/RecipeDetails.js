import React from "react";
import './css/recipe-details.css'
import {Link} from "react-router-dom";

class RecipeDetails extends React.Component {
  componentDidMount() {
    const {params} = this.props.match
    console.log(params)
  }

  render() {
    const recipe = this.props.location.state.recipe
    console.log(recipe)
    return (
      <div className="recipe-details">
        <Link to={{pathname: `/`,}}> <p className="back-link">Back</p></Link>
        <h1 className="recipe-details-header">{recipe.label}</h1>

        <div className="img-container">
          <div className="border-corners">
            <img src={recipe.image} alt=""/>
          </div>
        </div>

        <div className="meta-details-container">
          <div className="meta-details-wrapper">
            <div className="meta-details">
              <h4>Prep time: {recipe.totalTime}</h4>
              <h4>Yield: {recipe.yield}</h4>
              <h4>Calories: {recipe.calories}</h4>
              <h4>Weight: {recipe.totalWeight}</h4>
            </div>
          </div>
        </div>

        <div className="ingredients-list-container">
          <div className="ingredients-list-wrapper">

            <div className="ingredients">
              <h1 className="sub-header">Ingredients</h1>

              <ol className="ingredients-list">
                {Object.keys(recipe.ingredients).map(key =>
                  <li className="ingredients-list-item">
                    {recipe.ingredients[key].text}
                  </li>
                )}
              </ol>
            </div>
          </div>
        </div>

        <div className="ingredients-list-container">
          <div className="ingredients-list-wrapper">

            <div className="ingredients">
              <h1 className="sub-header">Procedure</h1>

              <a href={recipe.url} target="_blank">Go to procedure</a>
            </div>
          </div>
        </div>

        <div className="ingredients-list-container">
          <div className="ingredients-list-wrapper">

            <div className="ingredients">
              <h1 className="sub-header">Nutrition Facts</h1>

              <ul className="ingredients-list">
                {Object.keys(recipe.totalNutrients).map(key =>
                  <li className="ingredients-list-item">
                    {/*<img className="recipe-details-image" src={recipe.ingredients[key].image} alt=""/>*/}
                    <span
                      className="nutrition"> {recipe.totalNutrients[key].label}:</span> {recipe.totalNutrients[key].quantity}{recipe.totalNutrients[key].unit}
                  </li>
                )}
              </ul>
            </div>
          </div>
        </div>

      </div>
    )
  }
}

export default RecipeDetails;