import React, {useEffect, useState} from 'react';
import Recipe from "./Recipe";
import Banner from "./Banner";
import './App.css';

const App = () => {
  const recipesRef = React.createRef()

  const APP_ID = "aa377ea3";
  const APP_KEY = "1bf65d5d2e2441d341397339e0996da6"

  const [recipes, setRecipes] = useState([]);
  const [query, setQuery] = useState('desserts')

  useEffect(() => {
    getRecipes();
  }, [query]);

  const getRecipes = async () => {
    const response = await fetch(
      `https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`
    );
    const data = await response.json();
    setRecipes(data.hits);
  };

  const accessQuery = (search) => {
    setQuery(search)
    scrollToRef(recipesRef)
  }

  const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop)

  return (
    <div className="App">
      <Banner getQuery={accessQuery}/>

      <div className="recipe-container" ref={recipesRef}>

        <div className="header-container">
          <h1 className="headers recipes-header"> {query}</h1>
        </div>

        {recipes.map(recipe => (
          <Recipe key={recipe.recipe.label} recipe={recipe.recipe}/>
        ))}

        <div className="header-container">
          <h1 className="headers recipes-header"> Taste the goodness.</h1>
        </div>
      </div>
    </div>
  );
}

export default App;
