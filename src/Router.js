import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import App from "./App";
import RecipeDetails from "./RecipeDetails";

const Router = () => (
    <BrowserRouter>
        <Switch>
            <Route path="/" exact component={App}/>
            <Route path="/recipe/:name"  component={RecipeDetails}/>
        </Switch>
    </BrowserRouter>
)

export default Router;