import React, {useState} from "react";
import './css/banner.css'

const Banner = ({ getQuery }) => {

  const [search, setSearch] = useState('');

  const updateSearch = e => {
    setSearch(e.target.value);
  }

  const getSearch = e => {
    e.preventDefault();
    getQuery(search)
    setSearch('');
  }

  return (
    <div>
      <div className="featured-recipe">
        <div className="recipe-1">
          <div className="banner-container">
            <h1 className="headers">Good Rock Recipes</h1>
            <h3>Satisfy your cravings with our amazing recipes.</h3>

            <div className="btn-container">

              <button className="custom-btn" onClick={() => getQuery('desserts')}>popular</button>
              <button className="custom-btn" onClick={() => getQuery('macaroons')}>macaroons</button>
              <button className="custom-btn" onClick={() => getQuery('cupcakes')}>cupcakes</button>
              <button className="custom-btn" onClick={() => getQuery('cakes')}>cakes</button>
              <button className="custom-btn" onClick={() => getQuery('sandwiches')}>sandwiches</button>
              <button className="custom-btn" onClick={() => getQuery('ice cream')}>ice cream</button>
            </div>

            <form className="search-form" onSubmit={getSearch}>
              <input className="search-bar" type="text" value={search} onChange={updateSearch} placeholder="Search for more"/>
            </form>
          </div>
        </div>

        <div className="recipe-2 d-sm-none">

        </div>
        <div className="recipe-3 d-sm-none">

        </div>
        <div className="recipe-4 d-xs-none">

        </div>
        <div className="recipe-5 d-xs-none">

        </div>
        <div className="recipe-6">

        </div>
      </div>
    </div>
  )
}

export default Banner;