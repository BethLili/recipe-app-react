import React from "react";
import {Link} from "react-router-dom";
import {slugify} from "./helpers";
import './css/recipe.css'

const Recipe = ({recipe}) => {
  return (
    <div className="recipe">
      <div className="recipe-wrapper">
        <div className="image">
          <img
            src={recipe.image}
            alt={recipe.label}/>
        </div>
        <Link to={{
          pathname: `/recipe/${slugify(recipe.label)}`,
          state: {recipe: recipe}
        }}> <h3>{recipe.label}</h3></Link>

      </div>
    </div>
  )
}

export default Recipe;